import io.restassured.response.Response;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import static io.restassured.path.json.JsonPath.from;

public class FixturesApiTest {
    private FixturesApi fixturesApi;

    @Before
    public void setUp(){
        fixturesApi = new FixturesApi();
    }

    @After
    public void tearDown() {
        if (fixturesApi.currentFixtureId != null) {
            fixturesApi.deleteFixture(fixturesApi.currentFixtureId);
        }
    }

    @Test
    public void retrieveAllFixturesTest() {
        Response resp =  fixturesApi.getAllFixtures();
        Assert.assertEquals(200, resp.getStatusCode());

        String content = resp.asString();
        List<HashMap> fixtures = from(content).get("");
        Assert.assertEquals(3, fixtures.size());

        for(HashMap fixture : fixtures){
            Assert.assertNotNull(fixture.get("fixtureId"));
        }
    }

    @Test
    public void getFixtureByIdTest(){
        // Create a new fixture
        Response create =  fixturesApi.createFixture();
        Assert.assertEquals(200, create.getStatusCode());
        System.out.println("Created fixture with fixtureId: " + fixturesApi.currentFixtureId);

        // Test that first object in teams has 'teamId' of 'HOME
        Response resp =  fixturesApi.getFixtureById(fixturesApi.currentFixtureId);
        HashMap fixture = from(resp.asString()).get();
        HashMap ffs = (HashMap) fixture.get("footballFullState");
        List<HashMap> teams = (List<HashMap>) ffs.get("teams");
        HashMap homeTeam;
        homeTeam = (HashMap) teams.toArray()[0];
        Assert.assertEquals("HOME", homeTeam.get("teamId"));
    }

    @Test
    public void createFixtureAndLatencyTest(){
        // Create a new fixture
        Response create =  fixturesApi.createFixture();
        Assert.assertEquals(200, create.getStatusCode());
        System.out.println("Created fixture with fixtureId: " + fixturesApi.currentFixtureId);

        Response resp;

        do {
            resp = fixturesApi.getFixtureById(fixturesApi.currentFixtureId);
        } while (resp.getStatusCode() == 404);
        Assert.assertEquals(200, resp.getStatusCode());
    }

    @Test
    public void createAndDeleteNewFixtureTest(){
        // Create a new fixture
        Response create =  fixturesApi.createFixture();
        Assert.assertEquals(200, create.getStatusCode());

        System.out.println("Created fixture with fixtureId: " + fixturesApi.currentFixtureId);

        Response resp;

        do {
            resp = fixturesApi.getFixtureById(fixturesApi.currentFixtureId);
        } while (resp.getStatusCode() == 404);

        // Delete the newly created fixture
        Response delete = fixturesApi.deleteFixture(fixturesApi.currentFixtureId);
        Assert.assertEquals(200, delete.getStatusCode());
        Assert.assertEquals("Fixture has been deleted", delete.asString());
    }
}
