import org.json.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.*;

class FixturesApi {

    private RequestSpecification request;
    String currentFixtureId;

    FixturesApi(){
        baseURI = "http://localhost:3000";
        request = RestAssured.given().contentType("application/json");
    }

    Response getAllFixtures(){
        return request.get("/fixtures");
    }

    Response getFixtureById(String id) {
        System.out.println("Getting fixture with fixtureId ==> " + id);
        return request.get("/fixture/" + id);
    }

    Response createFixture(){
        JSONObject payload = new FixturesModel().generateFixture();
        System.out.println("Sending fixture ==> " + payload.toString());
        currentFixtureId = payload.getString("fixtureId");
        request.body(payload.toString());
        return request.post("/fixture");
    }

    Response deleteFixture(String id){
        System.out.println("Deleting fixture with fixtureId ==> " + id);
        return request.delete("/fixture/" + id);
    }
}
