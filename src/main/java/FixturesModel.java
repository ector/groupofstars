import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Random;

class FixturesModel {
    private Random rand = new Random();

    JSONObject generateFixture() {

    JSONObject fixture = new JSONObject();
    fixture.put("fixtureId", generateFixtureId());

    JSONObject fixtureStatus = new JSONObject();
    fixtureStatus.put("displayed", true);
    fixtureStatus.put("suspended", false);
    fixture.put("fixtureStatus", fixtureStatus);

    JSONArray messages = new JSONArray();

    JSONObject footballFullState = new JSONObject();
    footballFullState.put("homeTeam", "Chelsea");
    footballFullState.put("awayTeam", "Man City");
    footballFullState.put("finished", false);
    footballFullState.put("gameTimeInSeconds", rand.nextInt(2600));
    footballFullState.put("goals", messages);
    footballFullState.put("period", "FIRST_HALF");
    footballFullState.put("possibles", messages);
    footballFullState.put("corners", messages);
    footballFullState.put("redCards", messages);
    footballFullState.put("yellowCards", messages);
    footballFullState.put("startDateTime", "2018-03-20T10:49:38.655Z");
    footballFullState.put("started", true);

    JSONArray team_data = new JSONArray();
    JSONObject h_team = new JSONObject();
    JSONObject a_team = new JSONObject();
    h_team.put("association","HOME");
    h_team.put("name","Chelsea");
    h_team.put("teamId","HOME");
    team_data.put(h_team);
    a_team.put("association","AWAY");
    a_team.put("name","Man City");
    a_team.put("teamId","AWAY");
    team_data.put(a_team);
    footballFullState.put("teams", team_data);
    fixture.put("footballFullState", footballFullState);

    return fixture;
    }

    private String generateFixtureId(){

        int fixtureId;
        do {
            fixtureId = rand.nextInt(50);
        } while (fixtureId <=3);
        return String.valueOf(fixtureId);
    }
}
