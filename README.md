# The Star Group

This file contains the solution to the questions given

### Prerequisites

Ensure you have nodejs, java jdk and maven installed on your machine

### Run server
In the server project directory, run the command below

```
npm install && npm run start
```
you should see the following output

```
Server is listening on port 3000
```

To run the test, type the command below in this project base directory

```
mvn test
```

## Authors

* **Adetola** - [The Star Group](https://bitbucket.org/ector/groupofstars)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details